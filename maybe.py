from monad import *

class Maybe(Monad):	
	def __init__(self, value):
		super(Maybe, self).__init__()
		self.value = value

	def __rshift__(self, other):
		return other.function(self.value)

	def __lshift__(self, other):
		return self.value

	def __repr__(self):
		if self.value == None: return 'Nothing'
		else: return 'Just(%r)' % self.value
	
	@classmethod
	def unit(cls, value):
		return cls(value)

#unit is the Haskell return
unit = Maybe.unit

@monadic
def f(x): return unit(x+1)

@monadic
def g(x): return unit(x+2)

m = Maybe(1)
n = Maybe(None)

#showing the Haskell >>= operator
res = m >>~ f >>~ g
print res
#emit -> Just(4)

#showing the Haskell >> operator
res = n << f
print res
#emit -> Nothing
