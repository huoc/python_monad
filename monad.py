class Monad(object):
	def __init__(self, function=lambda x: x):
		self.function = function

	def __rshift__(self):
		raise NotImplementedError

	def __invert__(self):
		return self

def monadic(f):
	return Monad(f)
